package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIndex(t *testing.T) {
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}
	w := httptest.NewRecorder()
	handler := http.HandlerFunc(Index)

	handler.ServeHTTP(w, req)
	status := w.Code
	if status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

}

func TestHealthCheck(t *testing.T) {
	// create request to pass to our handler with no query parameter
	req, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Fatal(err)
	}

	// create response recorder which satisfies http.responsewriter to record the response
	w := httptest.NewRecorder()
	handler := http.HandlerFunc(HealthCheck)

	// our handler satisfy http.Handler, so we can call their ServeHTTP Method
	// directly and pass in our Request and ResponseRecorder
	handler.ServeHTTP(w, req)

	// check the status code is what we expect
	status := w.Code
	if status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// check the response body is what we expect.
	expected := `{"Status":"good"}`
	if w.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v", w.Body.String(), expected)
	}
}
